﻿using Map.Entities;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
    public Image icon;
    public Button removeButton;

    Product item;

    public void AddItem(Product newItem)
    {
        item = newItem;
        
        switch (newItem.name.ToLower())
        {
            case "pois":
                icon.sprite = Inventory.instance.petitpois;
                break;
            case "lait":
                icon.sprite = Inventory.instance.lait;
                break;
            case "sugar":
                icon.sprite = Inventory.instance.sucre;
                break;
            case "pommme":
                icon.sprite = Inventory.instance.pomme;
                break;
            case "cereal":
                icon.sprite = Inventory.instance.cereales;
                break;
            case "can":
                icon.sprite = Inventory.instance.can;
                break;
            case "ravioli":
                icon.sprite = Inventory.instance.raviole;
                break;
            default:
                icon.sprite = Inventory.instance.cube;
                break;
        }
        icon.enabled = true;
        removeButton.interactable = true;
    }

    public void ClearSlot()
    {
        item = null;

        icon.sprite = null;
        icon.enabled = false;
        removeButton.interactable = false;
    }

    public void OnRemoveButton ()
    {
        Inventory.instance.Remove(item);
    }
}
