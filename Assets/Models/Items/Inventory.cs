﻿using System.Collections;
using System.Collections.Generic;
using Map.Entities;
using UnityEngine;

public class Inventory : MonoBehaviour
{

    #region Singleton

    public static Inventory instance;
    public Sprite lait = null;
    public Sprite pomme = null;
    public Sprite cereales = null;
    public Sprite petitpois = null;
    public Sprite sucre = null;
    public Sprite cube = null;
    public Sprite can = null;
    public Sprite raviole = null;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of Inventory found!");
            return;
        }

        instance = this;
    }

    #endregion

    // Callback which is triggered when
    // an item gets added/removed.
    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    public int space = 20;  // Amount of slots in inventory

    // Current list of items in inventory
    public List<Product> items = new List<Product>();

    // Add a new item. If there is enough room we
    // return true. Else we return false.
    public bool Add(Product item)
    {
        // Don't do anything if it's a default item
        // Check if out of space
        if (items.Count >= space)
        {
            Debug.Log("Not enough room.");
            return false;
        }
        items.Add(item);    // Add item to list
        // Trigger callback
        if (onItemChangedCallback != null)
            onItemChangedCallback.Invoke();

        return true;
    }

    // Remove an item
    public void Remove(Product item)
    {
        items.Remove(item);     // Remove item from list

        // Trigger callback
        if (onItemChangedCallback != null)
            onItemChangedCallback.Invoke();
    }

}