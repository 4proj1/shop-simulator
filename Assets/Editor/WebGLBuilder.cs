﻿using UnityEditor;
class WebGLBuilder {
    static void build() {

        // Place all your scenes here
        string[] scenes = {"Assets/FPS/Scenes/MainScene.unity", 
            "Assets/FPS/Scenes/IntroMenu.unity",
            "Assets/FPS/Scenes/LoseScene.unity",
            "Assets/FPS/Scenes/SecondaryScene.unity",
            "Assets/FPS/Scenes/WinScene.unity"};

        string pathToDeploy = "builds/WebGLversion/";       

        BuildPipeline.BuildPlayer(scenes, pathToDeploy, BuildTarget.WebGL, BuildOptions.None);      
    }
}