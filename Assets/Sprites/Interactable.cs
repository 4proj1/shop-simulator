﻿using UnityEngine;

/*	
	This component is for all objects that the player can
	interact with such as enemies, items etc. It is meant
	to be used as a base class.
*/

public class Interactable : MonoBehaviour
{

    public float radius = 3f;               // How close do we need to be to interact?

    public PlayerCharacterController m_PlayerCharacterController;       // Reference to the player transfor

    public virtual void Interact()
    {
        Debug.Log("Interacting with " + transform.name);
    }

    void Update()
    {
        // If we are close enough
        float distance = Vector3.Distance(m_PlayerCharacterController.transform.position, transform.position);
        if (distance <= radius)
        {
            // Interact with the object
            Interact();
        }
    }

    // Draw our radius in the editor
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
