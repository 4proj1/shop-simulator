﻿using Map.Entities;
using UnityEngine;

public class ItemPickup : Interactable
{

    public Product item;

    public override void Interact()
    {
        base.Interact();
        //Debug.Log(m_PlayerCharacterController.transform.position);
        PickUp();
    }

    void PickUp()
    {
        Debug.Log("Picking up" + item.name);
        bool wasPickedUp = Inventory.instance.Add(item);

        if (wasPickedUp)
        {
            Destroy(gameObject);
        }
    }
}
