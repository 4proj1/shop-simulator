﻿using DefaultNamespace;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.UIElements;

[RequireComponent(typeof(CharacterController), typeof(PlayerInputHandler), typeof(AudioSource))]
public class PlayerCharacterController : MonoBehaviour
{
    [Header("References")]
    [Tooltip("Reference to the main camera used for the player")]
    public Camera playerCamera;

    [Header("Rotation")]
    [Tooltip("Rotation speed for moving the camera")]
    public float rotationSpeed = 200f;
    
    [Header("Movement")]
    [Tooltip("Movement speed")]
    public float movementSpeed = 0.3f;
    
    public float speed = 5; // units per second
    public float turnSpeed = 90; // degrees per second
    public float maxPickRange = 3;
    public GameObject productFacingPanel;
    
    public Vector3 characterVelocity { get; set; }
    public float RotationMultiplier
    {
        get
        {
            return 1f;
        }
    }
    
    PlayerInputHandler m_InputHandler;
    CharacterController m_Controller;
    Actor m_Actor;
    Vector3 m_CharacterVelocity;
    float m_LastTimeJumped = 0f;
    float m_CameraVerticalAngle = 0f;
    float m_footstepDistanceCounter;
    private Text _productFacingText;

    void Start()
    {
        _productFacingText = productFacingPanel.transform.GetChild(0).gameObject.GetComponent<Text>();
        // fetch components on the same gameObject
        m_Controller = GetComponent<CharacterController>();
        DebugUtility.HandleErrorIfNullGetComponent<CharacterController, PlayerCharacterController>(m_Controller, this, gameObject);

        m_InputHandler = GetComponent<PlayerInputHandler>();
        DebugUtility.HandleErrorIfNullGetComponent<PlayerInputHandler, PlayerCharacterController>(m_InputHandler, this, gameObject);

        m_Actor = GetComponent<Actor>();
        DebugUtility.HandleErrorIfNullGetComponent<Actor, PlayerCharacterController>(m_Actor, this, gameObject);

        m_Controller.enableOverlapRecovery = true;
    }

    void Update()
    {
        HandleCharacterMovement();
        HandlePickUp();
    }

    void HandlePickUp()
    {
        RaycastHit hit;
 
        if(Physics.Raycast(playerCamera.transform.position, playerCamera.transform.forward, out hit, maxPickRange))
        {
            ShelfBehaviour shelfBehaviour = hit.transform.gameObject.GetComponent<ShelfBehaviour>();
            if (shelfBehaviour != null)
            {
                if (m_InputHandler.PutKeyPressed())
                {
                    shelfBehaviour.Put();
                }
                if (m_InputHandler.PickµUpKeyPressed())
                {
                    shelfBehaviour.PickUp();
                }

                string productName = shelfBehaviour.GetProductName();
                if (productName.Length > 20)
                {
                    productName = productName.Substring(0, 20) + "...";
                }

                _productFacingText.text = productName;
                productFacingPanel.SetActive(true);
                Debug.DrawRay(playerCamera.transform.position, playerCamera.transform.forward * maxPickRange, Color.yellow);
            }
            else
            {
                productFacingPanel.SetActive(false);
                Debug.DrawRay(playerCamera.transform.position, playerCamera.transform.forward * maxPickRange, Color.red);
            }
        }
        else
        {
            productFacingPanel.SetActive(false);
            Debug.DrawRay(playerCamera.transform.position, playerCamera.transform.forward * maxPickRange, Color.blue);
        }
    }
    
    void HandleCharacterMovement()
    {
        // horizontal character rotation
        transform.Rotate(new Vector3(0f, (m_InputHandler.GetLookInputsHorizontal() * rotationSpeed * RotationMultiplier), 0f), Space.Self);

        // add vertical inputs to the camera's vertical angle
        m_CameraVerticalAngle += m_InputHandler.GetLookInputsVertical() * rotationSpeed * RotationMultiplier;
        // limit the camera's vertical angle to min/max
        m_CameraVerticalAngle = Mathf.Clamp(m_CameraVerticalAngle, -89f, 89f);
        // apply the vertical angle as a local rotation to the camera transform along its right axis (makes it pivot up and down)
        playerCamera.transform.localEulerAngles = new Vector3(m_CameraVerticalAngle, 0, 0);

        // apply the final calculated velocity value as a character movement
        Vector3 capsuleBottomBeforeMove = GetCapsuleBottomHemisphere();
        Vector3 capsuleTopBeforeMove = GetCapsuleTopHemisphere(m_Controller.height);
        // m_Controller.Move(characterVelocity * Time.deltaTime);

        // detect obstructions to adjust velocity accordingly
        if (Physics.CapsuleCast(capsuleBottomBeforeMove, capsuleTopBeforeMove, m_Controller.radius, characterVelocity.normalized, out RaycastHit hit, characterVelocity.magnitude * Time.deltaTime, -1, QueryTriggerInteraction.Ignore))
        {
            // We remember the last impact speed because the fall damage logic might need it
            characterVelocity = Vector3.ProjectOnPlane(characterVelocity, hit.normal);
        }
        
        // transform.Rotate(0, Input.GetAxis("Horizontal") * turnSpeed * Time.deltaTime, 0);
        Vector3 vel = transform.forward * (Input.GetAxis("Vertical") * speed);
        Vector3 vel2 = transform.right * (Input.GetAxis("Horizontal") * speed);
        m_Controller.SimpleMove(vel);
        m_Controller.SimpleMove(vel2);
    }

    // Gets the center point of the bottom hemisphere of the character controller capsule    
    Vector3 GetCapsuleBottomHemisphere()
    {
        return transform.position + (transform.up * m_Controller.radius);
    }

    // Gets the center point of the top hemisphere of the character controller capsule    
    Vector3 GetCapsuleTopHemisphere(float atHeight)
    {
        return transform.position + (transform.up * (atHeight - m_Controller.radius));
    }
    
}
