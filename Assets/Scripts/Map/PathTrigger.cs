﻿using System;
using UnityEngine;
using UnityEngine.Networking;

namespace Map
{
    public class PathTrigger : MonoBehaviour
    {
        public int id;
        public int x;
        public int y;
        
        private void OnTriggerEnter(Collider other)
        {
            string iaUrl = GameEnvironmentVars.iaURL;

            // string iaUrl = "localhost:8000";

            iaUrl += "/?id-user=" + GameEnvironmentVars.userId + "&x=" + x + "&y=" + y + "&timestamp=" + (Int64)System.DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;

            WWWForm form = new WWWForm();

            UnityWebRequest www = UnityWebRequest.Post(iaUrl, form);
            
            www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Form upload complete!");
            }
        }
    }
}