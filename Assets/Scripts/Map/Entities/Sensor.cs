﻿namespace Map.Entities
{
    [System.Serializable]
    public class Sensor
    {
        public int id;
        public string type;
        public string way;
        public int tile;
    }
}