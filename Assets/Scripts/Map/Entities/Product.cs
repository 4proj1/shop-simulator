﻿using UnityEngine;

namespace Map.Entities
{
    [System.Serializable]
    public class Product
    {
        public int id;
        public string name;
        public int weight;
        public int tile;
    }
}