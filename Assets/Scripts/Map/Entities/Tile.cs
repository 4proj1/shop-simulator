﻿namespace Map.Entities
{
    [System.Serializable]
    public class Tile
    {
        public int id;
        public int x;
        public int y;
        public string type;

        public Sensor[] sensors;
        public Product[] products;
        
    }
}