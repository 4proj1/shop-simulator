﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using DefaultNamespace;
using Map;
using Map.Entities;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class MapMaker : MonoBehaviour
{

    public int startX = 0;
    public int startY = 0;
    public int tileWidth = 0;

    public GameObject emptyShelfTile;
    public GameObject ravioliShelfTile;
    public GameObject cerealsShelfTile;
    public GameObject laitShelfTile;
    public GameObject canShelfTile;
    public GameObject poisShelfTile;
    public GameObject pommeShelfTile;
    public GameObject sugarShelfTile;
    public GameObject defaultShelfTile;
    public GameObject wallTile;
    public GameObject pathTile;
    
    // Start is called before the first frame update
    void Start()
    {
        // StartCoroutine(MakeMap("localhost:8001/tile"));
        StartCoroutine(MakeMap(GameEnvironmentVars.shopConfigUrl));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    
    IEnumerator MakeMap(string uri)
    {
        // Get tiles
        Wrapper wrapper = null;
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {

            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

            if (webRequest.isNetworkError)
            {
                Debug.Log(pages[page] + ": Error: " + webRequest.error);
            }
            else
            {
                Debug.Log(pages[page] + ":\nReceived: " + webRequest.downloadHandler.text);
                wrapper = JsonUtility.FromJson<Wrapper>("{ \"tiles\":" + webRequest.downloadHandler.text + "}");
            }
        }
        
        // Generate map

        foreach (Tile tile in wrapper.tiles)
        {
            switch (tile.type)
            {
                case "path":
                    PutPathInScene(tile.x, tile.y, 0.6f, 0.5f, 0.5f, pathTile);
                    break;
                case "wall":
                    PutInScene(tile.x, tile.y, 0.6f, 0.5f, 0.5f, wallTile);
                    break;
                case "weightSensor":
                case "shelf":
                    PutShelfInScene(0.6f, 0.5f, 0.5f, tile, GetNeighbours(tile, wrapper.tiles));
                    break;
            }
        }
    }

    void PutInScene(int mapX, int mapY, float heightDelta, float proportionX,  float proportionY, GameObject gameObject)
    {
        GameObject instance = Instantiate(gameObject);
        instance.transform.position = new Vector3(mapX * tileWidth + tileWidth * proportionX, heightDelta, mapY * tileWidth + tileWidth * proportionY);
    }
    void PutShelfInScene(float heightDelta, float proportionX,  float proportionY, Tile tile, List<Tile> neightbours)
    {
        GameObject instance;
        if (tile.products.Length > 0)
        {
            switch (tile.products[0].name.ToLower())
            {
                case "pois":
                    instance = Instantiate(poisShelfTile);
                    break;
                case "lait":
                    instance = Instantiate(laitShelfTile);
                    break;
                case "sugar":
                    instance = Instantiate(sugarShelfTile);
                    break;
                case "pommme":
                    instance = Instantiate(pommeShelfTile);
                    break;
                case "cereal":
                    instance = Instantiate(cerealsShelfTile);
                    break;
                case "can":
                    instance = Instantiate(canShelfTile);
                    break;
                case "ravioli":
                    instance = Instantiate(ravioliShelfTile);
                    break;
                default:
                    instance = Instantiate(defaultShelfTile);
                    break;
            }
            ShelfBehaviour shelfBehaviour = instance.GetComponent<ShelfBehaviour>();
            shelfBehaviour.product = tile.products[0];
            shelfBehaviour.tile = tile;
        }
        else
        {
            instance = Instantiate(emptyShelfTile);
        }
        instance.transform.position = new Vector3(tile.x * tileWidth + tileWidth * proportionX, heightDelta, tile.y * tileWidth + tileWidth * proportionY);
        instance.transform.Rotate(GetShelfRotationEulerVector(tile, neightbours));
    }

    private Vector3 GetShelfRotationEulerVector(Tile tile, List<Tile> neightbours)
    {
        List<Tile> paths = GetPathNextTo(tile, neightbours);
        if (paths.Count == 1)
        {
            if (paths[0].x == tile.x)
            {
                if (paths[0].y == tile.y - 1)
                {
                    return new Vector3(0, 90, 0);
                }
                if (paths[0].y == tile.y + 1)
                {
                    return new Vector3(0, -90, 0);
                }
            }
            if (paths[0].x == tile.x - 1)
            {
                return new Vector3(0, 180, 0);
            }
            if (paths[0].x == tile.x + 1)
            {
                return new Vector3();
            }
        }
        else
        {
            
        }
        return new Vector3();
    }

    private void PutPathInScene(int mapX, int mapY, float heightDelta, float proportionX,  float proportionY, GameObject gameObject)
    {
        if (mapX != -1 && mapY != -1)
        {
            if (mapX == 0)
            {
                PutPathInScene(-1, mapY, heightDelta, proportionX, proportionX, gameObject);
            }
            if (mapY == 0)
            {
                PutPathInScene(mapX, -1, heightDelta, proportionX, proportionX, gameObject);
            }
        }
        GameObject instance = Instantiate(gameObject);
        PathTrigger pathTrigger = instance.GetComponent<PathTrigger>();
        pathTrigger.x = mapY == -1 ? -1 : mapX;
        pathTrigger.y = mapX == -1 ? -1 : mapY;
        instance.transform.position = new Vector3(mapX * tileWidth + tileWidth * proportionX, heightDelta, mapY * tileWidth + tileWidth * proportionY);
    }

    private static List<Tile> GetPathNextTo(Tile tile, IEnumerable<Tile> neighbours)
    {
        List<Tile> res = new List<Tile>();

        foreach (Tile neighbour in neighbours)
        {
            if (neighbour.type == "path")
            {
                res.Add(neighbour);
            }
        }

        return res;
    }
    private static List<Tile> GetNeighbours(Tile tile, IReadOnlyList<Tile> tiles)
    {
        List<Tile> neighbours = new List<Tile>();
        int maxX = GetMaxX(tiles);
        int maxY = GetMaxY(tiles);

        if (tile.x > 0)
        {
            neighbours.Add(GetTileByCoords(tile.x - 1, tile.y, tiles));
        }

        if (tile.x < maxX)
        {
            neighbours.Add(GetTileByCoords(tile.x + 1, tile.y, tiles));
        }

        if (tile.y > 0)
        {
            neighbours.Add(GetTileByCoords(tile.x, tile.y - 1, tiles));
        }

        if (tile.y < maxY)
        {
            neighbours.Add(GetTileByCoords(tile.x, tile.y + 1, tiles));
        }

        return neighbours;
    }
    private static Tile GetTileByCoords(int x, int y, IReadOnlyList<Tile> tiles)
    {
        int maxX = GetMaxX(tiles);
        return tiles[x + y * (maxX+1)];
    }
    private static int GetMaxX(IReadOnlyList<Tile> tiles)
    {
        Tile lastTile = tiles[tiles.Count - 1];
        return lastTile.x;
    }
    private static int GetMaxY(IReadOnlyList<Tile> tiles)
    {
        Tile lastTile = tiles[tiles.Count - 1];
        return lastTile.y;
    }
}