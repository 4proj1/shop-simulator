﻿using System;
using Map;
using Map.Entities;
using UnityEngine;
using UnityEngine.Networking;

namespace DefaultNamespace
{
    public class ShelfBehaviour : MonoBehaviour
    {

        public Tile tile { get; set; }

        public Product product { get; set; }

        public void PickUp()
        {
            if (Inventory.instance.space <= Inventory.instance.items.Count)
            {
                return;
            }
            GameObject listItems = this.transform.GetChild(1).gameObject;

            bool picked = false;
            
            for (int i = 0; i < listItems.transform.childCount; i++)
            {
                Renderer renderer = listItems.transform.GetChild(i).gameObject.GetComponent<Renderer>();
                if (renderer.enabled)
                {
                    renderer.enabled = false;
                    picked = true;
                    break;
                }
            }

            if (picked)
            {
                Inventory.instance.Add(product);
                SendPickSensorMessage();
            }
        }

        public void Put()
        {
            GameObject listItems = this.transform.GetChild(1).gameObject;

            if (listItems.transform.GetChild(0).gameObject.GetComponent<Renderer>().enabled)
            {
                throw new Exception("No space left, cannot put " + GetProductName());
            }

            bool put = false;
            
            for (int i = 1; i < listItems.transform.childCount; i++)
            {
                if (listItems.transform.GetChild(i).gameObject.GetComponent<Renderer>().enabled)
                {
                    listItems.transform.GetChild(i-1).gameObject.GetComponent<Renderer>().enabled = true;
                    put = true;
                    break;
                }
            }

            if (!put)
            {
                listItems.transform.GetChild(listItems.transform.childCount-1).gameObject.GetComponent<Renderer>().enabled = true;
            }

            Inventory.instance.Remove(product);
            SendPutSensorMessage();
        }

        public void SendPutSensorMessage()
        {

            string iaUrl = GameEnvironmentVars.iaURL;
            // string iaUrl = "localhost:8000";

            iaUrl += "/?x=" + tile.x + "&y=" + tile.y + "&action=pose&timestamp=" + (Int64)System.DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;

            WWWForm form = new WWWForm();

            UnityWebRequest www = UnityWebRequest.Post(iaUrl, form);
            
            www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Form upload complete!");
            }
        }
        
        public void SendPickSensorMessage()
        {

            string iaUrl = GameEnvironmentVars.iaURL;
            // string iaUrl = "localhost:8000";

            iaUrl += "/?x=" + tile.x + "&y=" + tile.y + "&action=take&timestamp=" + (Int64)System.DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;

            WWWForm form = new WWWForm();

            UnityWebRequest www = UnityWebRequest.Post(iaUrl, form);
            
            www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Form upload complete!");
            }
        }

        public string GetProductName()
        {
            if (product != null)
            {
                return product.name;
            }

            return "Product";
        }
    }
}