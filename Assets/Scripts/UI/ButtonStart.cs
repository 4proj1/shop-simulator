﻿using Map;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI
{
    public class ButtonStart : MonoBehaviour
    {
        public void SetIdAndRunSimulation()
        {
            string userId = GameObject.Find("InputField").GetComponent<InputField>().text;
            GameEnvironmentVars.shopConfigUrl = GameObject.Find("InputField shop config").GetComponent<InputField>().text;
            GameEnvironmentVars.iaURL = GameObject.Find("InputField ia url").GetComponent<InputField>().text;
            GameEnvironmentVars.userId = userId;

            SceneManager.LoadScene("SampleScene");
        }
    }
}